package com.example.userloginapi

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.WindowManager
import java.security.AlgorithmParameterGenerator.getInstance
import java.util.Calendar.getInstance
import java.util.Currency.getInstance
import kotlinx.android.synthetic.main.activity_splash_screen.*


class SplashScreenActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
        gardenPic.alpha = 0f
        gardenPic.animate().setDuration(1500).alpha(1f).withEndAction {
            if (SharedPreferenceMnger.getInstance(this).isLoggedIn) {
                val intent = Intent(applicationContext,HomeActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
            }
            else if (!SharedPreferenceMnger.getInstance(this).isLoggedIn){
                val intent = Intent(applicationContext,MainActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
            }
        }
    }
    }
