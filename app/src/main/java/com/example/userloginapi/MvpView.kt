package com.example.userloginapi

interface MvpView {
    fun showLoading()
    fun hideLoading()
    fun isNetworkConnected():Boolean
    fun showMessage(message:String)
}