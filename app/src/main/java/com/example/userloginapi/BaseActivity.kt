package com.example.userloginapi

import android.content.Context
import android.net.ConnectivityManager
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

open class BaseActivity : AppCompatActivity(),MvpView {
    override fun isNetworkConnected() : Boolean {
        val connectivityManager=this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo=connectivityManager.activeNetworkInfo
        return  networkInfo?.isConnectedOrConnecting == true
    }

    override fun showLoading() {
        progressBarMainActivityLogin?.visibility = View.VISIBLE
    }
    override fun hideLoading() {
        progressBarMainActivityLogin?.visibility = View.GONE
    }

    override fun showMessage(message: String) {
        Toast.makeText(this,message, Toast.LENGTH_LONG).show()
    }
}