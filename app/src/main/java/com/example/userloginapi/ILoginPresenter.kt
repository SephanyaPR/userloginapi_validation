package com.example.userloginapi

interface ILoginPresenter {
    fun callLoginApi(emailId:String,password:String,providerType:Int)
}