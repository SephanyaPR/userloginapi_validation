package com.example.userloginapi

data class LoginResponse(val status:String?=null,
                         val message:String?=null,
                         val data:User?=null)

data class Error(val status: String?,val message: String?)