package com.example.userloginapi

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat.startActivity
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.security.AlgorithmParameters.getInstance


class MainActivity : BaseActivity(),ILoginView {

    val builder = GsonBuilder()
    val gson = builder.serializeNulls().create()
    private val loginPresenter = LoginPresenter(this,this)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        progressBarMainActivityLogin.visibility = View.GONE

        buttonLoginActivity.setOnClickListener {


            var email = editTextEmailloginActivity.text.toString().trim()
            var password = editTextTextPasswordLoginMainActivity.text.toString().trim()
            var userType: Int = 1

            loginPresenter.callLoginApi(email, password, userType)

        }
    }



        override fun onBackPressed() {
            val intent = Intent(Intent.ACTION_MAIN)
            intent.addCategory(Intent.CATEGORY_HOME)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        }
    override fun onSuccess(loginBase: LoginResponse) {
        showMessage(loginBase.message!!)
        if (loginBase.status=="success"){
            SharedPreferenceMnger.getInstance(applicationContext).saveUser(loginBase.data!!)
            val intent = Intent(applicationContext,HomeActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
        }
        else
        {
            showMessage("Something is Wrong")
        }

    }

    override fun onError(error: Error) {
        showMessage(error.message!!)
    }


    }



