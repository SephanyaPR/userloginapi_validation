package com.example.userloginapi

interface ILoginView:MvpView {
    fun onSuccess(loginBase:LoginResponse)

    fun onError(error: Error)

}